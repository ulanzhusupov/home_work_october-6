import "dart:io";
import 'dart:math';

void main(List<String> arguments) {
  
  generatePassword();

  bool isUserWantMore = true;

  do {
    stdout.write("Хотите ли вы сгенерировать еще один пароль?(yes/no) ");
    String yesNo = stdin.readLineSync()!;
    switch(yesNo) {
      case "yes": generatePassword();
      case "no": isUserWantMore = false;
      default: {
        print("Вы ввели не понятно что. Попробуйте еще раз!");
        break;
      }
    }

  } while(isUserWantMore);
}


void generatePassword() {
  stdout.write("Введите длину для нового пароля: ");
  int passwordLength = int.parse(stdin.readLineSync()!);
  
  String alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";

  String password = "";

  for(int i = 0; i < passwordLength; i++) {
    int strOrNum = Random().nextInt(2);
    if(strOrNum == 1) {
      int randomAlphIndex = Random().nextInt(52);
      password += alphabet[randomAlphIndex];
    } else {
      int randomNum = Random().nextInt(9);
      password += randomNum.toString();
    }
  }


  print(password);

}